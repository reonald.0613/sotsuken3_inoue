/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#define NS_LOG_APPEND_CONTEXT                                   \
if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; }

#define BUFFER_SIZE 50

#include "geogrid-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include <algorithm>
#include <limits>

#include "ns3/mobility-model.h"
#include "ns3/mobility-helper.h"
#include "ns3/vector.h"

#define NUM_NODES 300

int geocenter[NUM_NODES] = {};
int recvCount = 0;
int thop = 0;
int nhop = 0;

int controlcost2;
int totalcost2;

namespace ns3 {
    
    NS_LOG_COMPONENT_DEFINE ("GeogridRoutingProtocol");
    
    namespace geogrid {
        NS_OBJECT_ENSURE_REGISTERED (RoutingProtocol);
        
        const uint32_t RoutingProtocol::GEOGRID_PORT = 654;
        
        RoutingProtocol::RoutingProtocol ()
        {
            
        }
        
        TypeId
        RoutingProtocol::GetTypeId (void)
        {
            static TypeId tid = TypeId ("ns3::geogrid::RoutingProtocol")
            .SetParent<Ipv4RoutingProtocol> ()
            .SetGroupName ("Geogrid")
            .AddConstructor<RoutingProtocol> ()
            .AddAttribute ("UniformRv",
                           "Access to the underlying UniformRandomVariable",
                           StringValue ("ns3::UniformRandomVariable"),
                           MakePointerAccessor (&RoutingProtocol::m_uniformRandomVariable),
                           MakePointerChecker<UniformRandomVariable> ())
            ;
            return tid;
        }
        
        
        
        RoutingProtocol::~RoutingProtocol ()
        {
        }
        
        void
        RoutingProtocol::DoDispose ()
        {
            m_ipv4 = 0;
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter =
                 m_socketAddresses.begin (); iter != m_socketAddresses.end (); iter++)
            {
                iter->first->Close ();
            }
            m_socketAddresses.clear ();
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter2 =
                 m_socketSubnetBroadcastAddresses.begin (); iter2 != m_socketSubnetBroadcastAddresses.end (); iter2++)
            {
                iter2->first->Close ();
            }
            m_socketSubnetBroadcastAddresses.clear ();
            
            Ipv4RoutingProtocol::DoDispose ();
        }
        
        void
        RoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit) const
        {
            *stream->GetStream () << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
            << "; Time: " << Now ().As (unit)
            << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (unit)
            << ", GEOGRID Routing table" << std::endl;
            
            //Print routing table here.
            *stream->GetStream () << std::endl;
        }
        
        int64_t
        RoutingProtocol::AssignStreams (int64_t stream)
        {
            NS_LOG_FUNCTION (this << stream);
            m_uniformRandomVariable->SetStream (stream);
            return 1;
        }
        
        
        Ptr<Ipv4Route>
        RoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header,
                                      Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
        {
            
            std::cout<<"Route Ouput Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
            Ptr<Ipv4Route> route;
            
            if (!p)
            {
                std::cout << "loopback occured! in routeoutput";
                return route;// LoopbackRoute (header,oif);
                
            }
            
            if (m_socketAddresses.empty ())
            {
                sockerr = Socket::ERROR_NOROUTETOHOST;
                NS_LOG_LOGIC ("No zeal interfaces");
                std::cout << "RouteOutput No zeal interfaces!!, packet drop\n";
                
                Ptr<Ipv4Route> route;
                return route;
            }
            
            return route;
        }
        
        
        bool
        RoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                                     Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                                     MulticastForwardCallback mcb, LocalDeliverCallback lcb, ErrorCallback ecb)
        {
            
            std::cout<<"Route Input Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
            return true;
        }
        
        
        void
        RoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
        {
            NS_ASSERT (ipv4 != 0);
            NS_ASSERT (m_ipv4 == 0);
            m_ipv4 = ipv4;
        }
        
        void
        RoutingProtocol::NotifyInterfaceUp (uint32_t i)
        {
            NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ()
                             << " interface is up");
            Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
            Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
            if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
            {
                return;
            }
            // Create a socket to listen only on this interface
            Ptr<Socket> socket;
            
            socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
            NS_ASSERT (socket != 0);
            socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvGeogrid,this));
            socket->BindToNetDevice (l3->GetNetDevice (i));
            socket->Bind (InetSocketAddress (iface.GetLocal (), GEOGRID_PORT));
            socket->SetAllowBroadcast (true);
            socket->SetIpRecvTtl (true);
            m_socketAddresses.insert (std::make_pair (socket,iface));
            
            
            // create also a subnet broadcast socket
            socket = Socket::CreateSocket (GetObject<Node> (),
                                           UdpSocketFactory::GetTypeId ());
            NS_ASSERT (socket != 0);
            socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvGeogrid, this));
            socket->BindToNetDevice (l3->GetNetDevice (i));
            socket->Bind (InetSocketAddress (iface.GetBroadcast (), GEOGRID_PORT));
            socket->SetAllowBroadcast (true);
            socket->SetIpRecvTtl (true);
            m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));
            
            
            if (m_mainAddress == Ipv4Address ())
            {
                m_mainAddress = iface.GetLocal ();
            }
            
            NS_ASSERT (m_mainAddress != Ipv4Address ());
        }
        
        void
        RoutingProtocol::NotifyInterfaceDown (uint32_t i)
        {
            
        }
        
        void
        RoutingProtocol::NotifyAddAddress (uint32_t i, Ipv4InterfaceAddress address)
        {
            
        }
        
        void
        RoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
        {
            
        }
        
        void
        RoutingProtocol::DoInitialize (void)
        {
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            for (int i = 1; i < 7; i++)
            {
                Simulator::Schedule(Seconds(i), &RoutingProtocol::SendHello, this);
                Simulator::Schedule(Seconds(i), &RoutingProtocol::SendGate, this);
            }
            
            for (int i = 1; i < 7; i++)
            {
                if (id == 0)
                    Simulator::Schedule(Seconds(i), &RoutingProtocol::SendBroadcast, this);
            }
            
            //結果
            for (int i = 1; i < 7; i++)
            {
                if (id == 0)
                    Simulator::Schedule (Seconds (i), &RoutingProtocol::SimulationResult, this);
            }
            
        }
        
        void
        RoutingProtocol::Send (int des_id)
        {
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel> ();
            int MicroSeconds = Simulator::Now ().GetMicroSeconds ();
            m_start_time[des_id] = MicroSeconds;
        }
        
        // 結果出力
        void
        RoutingProtocol::SimulationResult (void)
        {
            std::cout << "time" << Simulator::Now ().GetSeconds () << "\n";
            printf("recv %d\n", recvCount);
            if (Simulator::Now ().GetSeconds () == SimStartTime + 22)
            {
                int sum_br = 0;
                std::cout << "\n\n----結果---------------------------\n\n";
                for (auto itr = m_start_time.begin (); itr != m_start_time.end (); itr++)
                {
                    std::cout << "des id " << itr->first << "送信開始時刻" << m_start_time[itr->first]
                    << "\n";
                }
                std::cout << "sum_br" << sum_br << "\n";
                double avehop = (double) thop / (double) nhop;
                std::cout << "平均ホップ数：" << avehop << "\n";
                std::cout << "recvCount" << recvCount << "\n";
                double packet_recv_rate = (double) recvCount / (double) m_start_time.size ();
                std::cout << "パケット到達率：" << packet_recv_rate << "\n";
                
            }
        }
        
        //sender's broadcast
        void
        RoutingProtocol::SendBroadcast(void)
        {
            totalcost2 += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (GEOGRIDTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                BroadcastHeader broadcastheader;
                broadcastheader.SetE_Start(0);
                broadcastheader.SetTime(Now());
                broadcastheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                broadcastheader.SetE_Dst(0);
                
                broadcastheader.SetInstruction(0);
                
                
                broadcastheader.SetHopCount(0);
                packet->AddHeader (broadcastheader);
                
                TypeHeader tHeader (BROADCASTTYPE);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, GEOGRID_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        void
        RoutingProtocol::RecvGeogrid (Ptr<Socket> socket)
        {
            Address sourceAddress;
            Ptr<Packet> packet = socket->RecvFrom (sourceAddress);
            InetSocketAddress inetSourceAddr = InetSocketAddress::ConvertFrom (sourceAddress);
            Ipv4Address sender = inetSourceAddr.GetIpv4 ();
            Ipv4Address receiver;
            
            if (m_socketAddresses.find (socket) != m_socketAddresses.end ())
                receiver = m_socketAddresses[socket].GetLocal ();
            
            
            TypeHeader tHeader (BROADCASTTYPE);
            packet->RemoveHeader (tHeader);
            
            switch(tHeader.Get()){
                case BROADCASTTYPE:
                    RecvBroadcast(packet, receiver, sender);
                    break;
                case BROADCASTTYPE2:
                    RecvBroadcast(packet, receiver, sender);
                    break;
                case HELLOTYPE:
                    RecvHello(packet,receiver,sender);
                    break;
                case GATETYPE:
                    RecvGate(packet, receiver, sender);
                    break;
                default:
                    std::cout << "Unknown Type received in " << receiver << " from " << sender << "\n";
                    break;
                    
            }
            
        }
        
        //sender's broadcast
        void
        RoutingProtocol::RecvBroadcast(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvBRO\n");
            uint16_t mypos_x, mypos_y;
            
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
            Vector mypos = mobility->GetPosition ();
            
            
            mypos_x = mypos.x;
            mypos_y = mypos.y;
            
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();

            if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                printf("----------------------------\n");
                printf("arrived destination node, complete.\n");
                BroadcastHeader broadcastheader;
                packet->RemoveHeader(broadcastheader);
                
                uint16_t lasthop = broadcastheader.GetHopCount();
                uint16_t lastsource = broadcastheader.GetSource();
                lasthop = lasthop + 1;
                
                thop = thop + lasthop;
                nhop++;
                
                printf("recvnodeID = %d\n", id);
                
                printf("lasthop = %d\n",lasthop);
                printf("lastsource = %d\n",lastsource);
                printf("----------------------------\n");
                
                if(id == 1){
                    recvCount++;
                }
                
            } else {
                BroadcastHeader broadcastheader;
                packet->RemoveHeader(broadcastheader);
                
                uint16_t hop = broadcastheader.GetHopCount();
 
                if(mypos_x >= 100 && mypos_x <= 860 && mypos_y >= 100 && mypos_y <= 860){
                    if(geocenter[id] == 1){

                            SendBroadcast2(hop+1);
                        
                            printf("recvbroadcast中継 nodeId = %d\n", id);
    
                    }
                }
            }
            
        }
        
        //node's broadcast
        void
        RoutingProtocol::SendBroadcast2(uint16_t hop)
        {
            totalcost2 += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (GEOGRIDTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                BroadcastHeader broadcastheader;
                broadcastheader.SetE_Start(0);
                broadcastheader.SetTime(Now());
                broadcastheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                broadcastheader.SetE_Dst(0);
                
                
                broadcastheader.SetHopCount(hop);
                //Ptr<Packet> packet = Create<Packet> ();
                packet->AddHeader (broadcastheader);
                
                TypeHeader tHeader (BROADCASTTYPE2);
                packet->AddHeader (tHeader);
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, GEOGRID_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        void
        RoutingProtocol::SendHello()
        {
            //printf("sendhello\n");
            totalcost2 += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
            {
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
                Vector pos = mobility->GetPosition ();
                HelloHeader helloHeader;
                Ptr<Packet> packet = Create<Packet> ();
                helloHeader.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
                helloHeader.SetPosition_X(pos.x);
                helloHeader.SetPosition_Y(pos.y);
                packet->AddHeader(helloHeader);
                
                TypeHeader tHeader (HELLOTYPE);
                packet->AddHeader (tHeader);
                
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0, 3000)));
                Simulator::Schedule (jitter, &RoutingProtocol::SendTo, this , socket, packet, destination);
                
            }
        }
        
        void
        RoutingProtocol::SendTo(Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination)
        {
            //printf("SendTo\n");
            socket->SendTo (packet, 0, InetSocketAddress (destination, GEOGRID_PORT));
        }
        
        void
        RoutingProtocol::RecvHello(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvhello\n");
            HelloHeader helloHeader;
            packet->RemoveHeader(helloHeader);
            uint16_t nodeId = helloHeader.GetNodeId();
            uint16_t pos_x = helloHeader.GetPosition_X();
            uint16_t pos_y = helloHeader.GetPosition_Y();
            
            auto itr = table.find(nodeId);
            if(itr != table.end()){
                table.erase(itr);
                table.insert(std::make_pair(nodeId, pos_x));
            } else{
                table.insert(std::make_pair(nodeId, pos_x));
            }
            
            auto itr2 = table2.find(nodeId);
            if(itr2 != table2.end()){
                table2.erase(itr2);
                table2.insert(std::make_pair(nodeId, pos_y));
            } else {
                table2.insert(std::make_pair(nodeId, pos_y));
            }
        }
        
        //GATEpacket send
        void
        RoutingProtocol::SendGate(void)
        {
            totalcost2 += 1;
            controlcost2  += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (GEOGRIDTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                GateHeader gateheader;
                gateheader.SetE_Start(0);
                gateheader.SetTime(Now());
                gateheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                gateheader.SetE_Dst(0);
                
                gateheader.SetInstruction(0);
                
                
                gateheader.SetHopCount(0);
                packet->AddHeader (gateheader);
                
                TypeHeader tHeader (GATETYPE);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, GEOGRID_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        //GATEpacket recv
        void
        RoutingProtocol::RecvGate(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvGATE\n");
            
            uint16_t mypos_x, mypos_y;
            
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
            Vector mypos = mobility->GetPosition ();
            
            mypos_x = mypos.x;
            mypos_y = mypos.y;
            
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                printf("----------------------------\n");
                printf("GATEarrived destination node, complete.\n");
                GateHeader gateheader;
                packet->RemoveHeader(gateheader);
                
                uint16_t lasthop = gateheader.GetHopCount();
                uint16_t lastsource = gateheader.GetSource();
                lasthop = lasthop + 1;
                
                printf("recvnodeID = %d\n", id);
                
                //printf("lasthop = %d\n",lasthop);
                printf("lastsource = %d\n",lastsource);
                printf("----------------------------\n");
                
            } else {
                GateHeader gateheader;
                packet->RemoveHeader(gateheader);
                
                
                auto itr103 = table.begin();
                auto itr104 = table2.begin();
                
                if(mypos_x >= 100 && mypos_x <= 900 && mypos_y >= 100 && mypos_y <= 900){
                    
                    for(int i = 0; i < 4; i++){   //for grid
                        for(int j = 0; j < 4; j++){
                            
                            // printf("(x,y)=(%d,%d)番目グリッド  左上(%d,%d)--右上(%d,%d)--左下(%d,%d)--右下(%d,%d)  中心座標C(%d,%d)\n",i+1, j+1, 100+i*190,100+j*190, 290+i*190,100+j*190, 100+i*190,290+j*190, 290+i*190, 290+j*190, ((290+i*190)+(100+i*190))/2, ((290+j*190)+(100+j*190))/2);
                            
                            
                            if(mypos_x >= 100+i*190 && mypos_x <= 290+i*190 && mypos_y >= 100+j*190 && mypos_y <= 290+j*190){
                                double mydistance =
                                std::sqrt ((((290+i*190)+(100+i*190))/2 - mypos_x) * (((290+i*190)+(100+i*190))/2 - mypos_x) + (((290+j*190)+(100+j*190))/2 - mypos_y) * (((290+j*190)+(100+j*190))/2 - mypos_y));
                                
                                /*
                                 if(mypos_x >= 10+i*19 && mypos_x <= 29+i*19 && mypos_y >= 10+j*19 && mypos_y <= 29+j*19){
                                 double mydistance =
                                 std::sqrt ((((29+i*19)+(10+i*19))/2 - mypos_x) * (((29+i*19)+(10+i*19))/2 - mypos_x) + (((29+j*19)+(10+j*19))/2 - mypos_y) * (((29+j*19)+(10+j*19))/2 - mypos_y));
                                 */
                                
                                double source_minLenge = 300;
                                uint16_t minId = 0;
                                
                                printf("ID:%d  my_dis = %f\n", id, mydistance);
                                printf("tablesize = %lu\n", table.size());
                                
                                if(table.size() > 0){
                                    while(1){
                                        uint16_t source_pos_x = itr103->second;
                                        uint16_t source_pos_y = itr104->second;
                                        uint16_t source_id = itr103->first;
                                        
                                        //printf("s_id = %d", source_id);
                                        //printf("  spos_x = %d", source_pos_x);
                                        //printf("  spos_y = %d\n", source_pos_y);
                                        
                                        
                                        double source_distance =
                                        std::sqrt ((((290+i*190)+(100+i*190))/2 - source_pos_x) * (((290+i*190)+(100+i*190))/2 - source_pos_x) + (((290+j*190)+(100+j*190))/2 - source_pos_y) * (((290+j*190)+(100+j*190))/2 - source_pos_y));
                                        
    
                                        /*
                                         double source_distance =
                                         std::sqrt ((((29+i*19)+(10+i*19))/2 - source_pos_x) * (((29+i*19)+(10+i*19))/2 - source_pos_x) + (((29+j*19)+(10+j*19))/2 - source_pos_y) * (((29+j*19)+(10+j*19))/2 - source_pos_y));
                                         */
                                        
                                        //printf("ID:%d  source_dis = %f\n", source_id, source_distance);
                                        
                                        if(source_distance <= source_minLenge){
                                            source_minLenge = source_distance;
                                            minId = source_id;
                                            //printf("Minsource_dis = %f, ID = %d\n", source_minLenge, minId);
                                        }
                                        
                                        
                                        itr103++;
                                        itr104++;
                                        
                                        if(itr103 == table.end())
                                            break;
                                    }
                                } //if tablesize
                                

                                if(mydistance <= source_minLenge){
                                    //printf("Mydis_dis = %f, mindis = %f\n", mydistance, source_minLenge);
                                    source_minLenge = mydistance;
                                    //printf("MyId = %d, minId = %d\n", id, minId);
                                    minId = id;
                                    printf("minID = %d, minDis = %f\n", minId, source_minLenge);
                                    
                                    printf("グリッド座標(%d, %d)\n", i+1, j+1);
                                    
                                    geocenter[id] = 1;
                                    
                                }else{
                                    printf("minID = %d, minDis = %f\n", minId, source_minLenge);
                                    //printf("dis_dis = %f, mindis = %f\n", mydistance, source_minLenge);
                                }
                                
                            }
                        }
                    }
                    //for grid
                    
                    printf("SENDGATErecvbroadcast中継 nodeId = %d\n", id);
                    printf("----------------\n");
                    
                }
            }
            
        }
        
        std::map<int, int> RoutingProtocol::m_start_time;
        
    } //namespace geogrid
} //namespace ns3
