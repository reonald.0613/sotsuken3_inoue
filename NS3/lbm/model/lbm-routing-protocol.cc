/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#define NS_LOG_APPEND_CONTEXT                                   \
if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; }

#define BUFFER_SIZE 50

#include "lbm-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include <algorithm>
#include <limits>

#include "ns3/mobility-model.h"
#include "ns3/mobility-helper.h"
#include "ns3/vector.h"

int recvCount = 0;
int thop = 0;
int nhop = 0;

namespace ns3 {
    
    NS_LOG_COMPONENT_DEFINE ("LbmRoutingProtocol");
    
    namespace lbm {
        NS_OBJECT_ENSURE_REGISTERED (RoutingProtocol);
        
        /// UDP Port for LBM control traffic
        const uint32_t RoutingProtocol::LBM_PORT = 654;
        
        
        RoutingProtocol::RoutingProtocol ()
        {
            
        }
        
        TypeId
        RoutingProtocol::GetTypeId (void)
        {
            static TypeId tid = TypeId ("ns3::lbm::RoutingProtocol")
            .SetParent<Ipv4RoutingProtocol> ()
            .SetGroupName ("Lbm")
            .AddConstructor<RoutingProtocol> ()
            .AddAttribute ("UniformRv",
                           "Access to the underlying UniformRandomVariable",
                           StringValue ("ns3::UniformRandomVariable"),
                           MakePointerAccessor (&RoutingProtocol::m_uniformRandomVariable),
                           MakePointerChecker<UniformRandomVariable> ())
            ;
            return tid;
        }
        
        
        
        RoutingProtocol::~RoutingProtocol ()
        {
        }
        
        void
        RoutingProtocol::DoDispose ()
        {
            m_ipv4 = 0;
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter =
                 m_socketAddresses.begin (); iter != m_socketAddresses.end (); iter++)
            {
                iter->first->Close ();
            }
            m_socketAddresses.clear ();
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter2 =
                 m_socketSubnetBroadcastAddresses.begin (); iter2 != m_socketSubnetBroadcastAddresses.end (); iter2++)
            {
                iter2->first->Close ();
            }
            m_socketSubnetBroadcastAddresses.clear ();
            
            Ipv4RoutingProtocol::DoDispose ();
        }
        
        
        void
        RoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit) const
        {
            *stream->GetStream () << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
            << "; Time: " << Now ().As (unit)
            << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (unit)
            << ", LBM Routing table" << std::endl;
            
            //Print routing table here.
            *stream->GetStream () << std::endl;
        }
        
        int64_t
        RoutingProtocol::AssignStreams (int64_t stream)
        {
            NS_LOG_FUNCTION (this << stream);
            m_uniformRandomVariable->SetStream (stream);
            return 1;
        }
        
        
        
        Ptr<Ipv4Route>
        RoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header,
                                      Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
        {
            
            std::cout<<"Route Ouput Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
            Ptr<Ipv4Route> route;
            
            if (!p)
            {
                std::cout << "loopback occured! in routeoutput";
                return route;// LoopbackRoute (header,oif);
                
            }
            
            if (m_socketAddresses.empty ())
            {
                sockerr = Socket::ERROR_NOROUTETOHOST;
                NS_LOG_LOGIC ("No zeal interfaces");
                std::cout << "RouteOutput No zeal interfaces!!, packet drop\n";
                
                Ptr<Ipv4Route> route;
                return route;
            }
            
            
            return route;
        }
        
        
        bool
        RoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                                     Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                                     MulticastForwardCallback mcb, LocalDeliverCallback lcb, ErrorCallback ecb)
        {
            
            std::cout<<"Route Input Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
            return true;
        }
        
        
        
        void
        RoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
        {
            NS_ASSERT (ipv4 != 0);
            NS_ASSERT (m_ipv4 == 0);
            m_ipv4 = ipv4;
        }
        
        void
        RoutingProtocol::NotifyInterfaceUp (uint32_t i)
        {
            NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ()
                             << " interface is up");
            Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
            Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
            if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
            {
                return;
            }
            // Create a socket to listen only on this interface
            Ptr<Socket> socket;
            
            socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
            NS_ASSERT (socket != 0);
            socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvLbm,this));
            socket->BindToNetDevice (l3->GetNetDevice (i));
            socket->Bind (InetSocketAddress (iface.GetLocal (), LBM_PORT));
            socket->SetAllowBroadcast (true);
            socket->SetIpRecvTtl (true);
            m_socketAddresses.insert (std::make_pair (socket,iface));
            
            
            // create also a subnet broadcast socket
            socket = Socket::CreateSocket (GetObject<Node> (),
                                           UdpSocketFactory::GetTypeId ());
            NS_ASSERT (socket != 0);
            socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvLbm, this));
            socket->BindToNetDevice (l3->GetNetDevice (i));
            socket->Bind (InetSocketAddress (iface.GetBroadcast (), LBM_PORT));
            socket->SetAllowBroadcast (true);
            socket->SetIpRecvTtl (true);
            m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));
            
            
            if (m_mainAddress == Ipv4Address ())
            {
                m_mainAddress = iface.GetLocal ();
            }
            
            NS_ASSERT (m_mainAddress != Ipv4Address ());
        }
        
        void
        RoutingProtocol::NotifyInterfaceDown (uint32_t i)
        {
            
        }
        
        void
        RoutingProtocol::NotifyAddAddress (uint32_t i, Ipv4InterfaceAddress address)
        {
            
        }
        
        void
        RoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
        {
            
        }
        
        
        void
        RoutingProtocol::DoInitialize (void)
        {
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            for (int i = 1; i < 7; i++)
            {
                if (id == 0)
                    Simulator::Schedule(Seconds(i), &RoutingProtocol::SendBroadcast, this);
            }
            
            //結果
            for (int i = 1; i < 7; i++)
            {
                if (id == 0)
                    Simulator::Schedule (Seconds (i), &RoutingProtocol::SimulationResult, this);
            }
            
        }
        
        // 結果出力
        void
        RoutingProtocol::SimulationResult (void)
        {
            std::cout << "time" << Simulator::Now ().GetSeconds () << "\n";
            printf("recv %d\n", recvCount);
            if (Simulator::Now ().GetSeconds () == SimStartTime + 22)
            {
                int sum_br = 0;
                std::cout << "\n\n----結果---------------------------\n\n";
                for (auto itr = m_start_time.begin (); itr != m_start_time.end (); itr++)
                {
                    std::cout << "des id " << itr->first << "送信開始時刻" << m_start_time[itr->first]
                    << "\n";
                }
                std::cout << "sum_br" << sum_br << "\n";
                double avehop = (double) thop / (double) nhop;
                std::cout << "平均ホップ数：" << avehop << "\n";
                std::cout << "recvCount" << recvCount << "\n";
                double packet_recv_rate = (double) recvCount / (double) m_start_time.size ();
                std::cout << "パケット到達率：" << packet_recv_rate << "\n";
                
            }
        }
        
        
        //sender's broadcast
        void
        RoutingProtocol::SendBroadcast(void)
        {
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> (1024);
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (LBMTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                BroadcastHeader broadcastheader;
                broadcastheader.SetE_Start(0);
                broadcastheader.SetTime(Now());
                broadcastheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                broadcastheader.SetE_Dst(0);
                
                broadcastheader.SetInstruction(0);
                
                
                broadcastheader.SetHopCount(0);
                packet->AddHeader (broadcastheader);
                
                TypeHeader tHeader (BROADCASTTYPE);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, LBM_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        void
        RoutingProtocol::RecvLbm (Ptr<Socket> socket)
        {
            Address sourceAddress;
            Ptr<Packet> packet = socket->RecvFrom (sourceAddress);
            InetSocketAddress inetSourceAddr = InetSocketAddress::ConvertFrom (sourceAddress);
            Ipv4Address sender = inetSourceAddr.GetIpv4 ();
            Ipv4Address receiver;
            
            if (m_socketAddresses.find (socket) != m_socketAddresses.end ())
                receiver = m_socketAddresses[socket].GetLocal ();
            
            
            TypeHeader tHeader (BROADCASTTYPE);
            packet->RemoveHeader (tHeader);
            
            switch(tHeader.Get()){
                case BROADCASTTYPE:
                    RecvBroadcast(packet, receiver, sender);
                    break;
                case BROADCASTTYPE2:
                    RecvBroadcast(packet, receiver, sender);
                    break;
                case HELLOTYPE:
                    RecvHello(packet,receiver,sender);
                    break;
                default:
                    std::cout << "Unknown Type received in " << receiver << " from " << sender << "\n";
                    break;
                    
            }
            
        }
        
        //sender's broadcast
        void
        RoutingProtocol::RecvBroadcast(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            uint16_t mypos_x, mypos_y;
            
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
            Vector mypos = mobility->GetPosition ();
            
            
            mypos_x = mypos.x;
            mypos_y = mypos.y;
            
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                printf("----------------------------\n");
                printf("arrived destination node, complete.\n");
                BroadcastHeader broadcastheader;
                packet->RemoveHeader(broadcastheader);
                
                uint16_t lasthop = broadcastheader.GetHopCount();
                uint16_t lastsource = broadcastheader.GetSource();
                lasthop = lasthop + 1;
                
                thop = thop + lasthop;
                nhop++;
                
                printf("recvnodeID = %d\n", id);
                
                printf("lasthop = %d\n",lasthop);
                printf("lastsource = %d\n",lastsource);
                printf("----------------------------\n");
                
                if(id == 1){
                    recvCount++;
                }
                
            } else {
                BroadcastHeader broadcastheader;
                packet->RemoveHeader(broadcastheader);
                
                uint16_t hop = broadcastheader.GetHopCount();
                
                
                if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                    SendBroadcast2(hop+1);
                }
            }
            
        }
        
        //node's broadcast
        void
        RoutingProtocol::SendBroadcast2(uint16_t hop)
        {
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (LBMTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                BroadcastHeader broadcastheader;
                broadcastheader.SetE_Start(0);
                broadcastheader.SetTime(Now());
                broadcastheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                broadcastheader.SetE_Dst(0);
                
                
                broadcastheader.SetHopCount(hop);
                //Ptr<Packet> packet = Create<Packet> ();
                packet->AddHeader (broadcastheader);
                
                TypeHeader tHeader (BROADCASTTYPE2);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, LBM_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        void
        RoutingProtocol::SendTo(Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination)
        {
            //printf("SendTo\n");
            socket->SendTo (packet, 0, InetSocketAddress (destination, LBM_PORT));
        }
        
        std::map<int, int> RoutingProtocol::m_start_time;
        
    } //namespace lbm
} //namespace ns3
